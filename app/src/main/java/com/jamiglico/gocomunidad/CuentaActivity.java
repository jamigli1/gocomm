package com.jamiglico.gocomunidad;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.transition.Fade;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jamiglico.gocomunidad.Utils.AppConstants;
import com.jamiglico.gocomunidad.Utils.DrawerCreator;
import com.jamiglico.gocomunidad.Utils.Pkdx;
import com.jamiglico.gocomunidad.Utils.PrincipalMethods;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.UUID;

public class CuentaActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout headeCuenta;
    private ImageView teamIcon;

    private DatabaseReference mReference;
    private FirebaseDatabase mDatabase;
    private FirebaseAuth auth;
    private FirebaseUser user;

    private Toolbar toolbar;

    private TextView c_amigos, c_match, c_eventos;
    private TextView accounts;

    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;

    private View formAccount, formMatch;
    private EditText cuenta, nivel, codigo, descripcion;
    private TextView pokelist;
    private Spinner equipo, poke;

    private Button pick;

    private Uri uri;

    private int cuentapkmn;

    private SwipeRefreshLayout swipeRefreshLayout;

    FloatingActionMenu floatingActionMenu;
    FloatingActionButton f1, f2, f3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta);

        toolbar = (Toolbar) findViewById(R.id.toolbarCuenta);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerCreator.createDrawer(this,toolbar);

        initElementos();

        getFirebaseData();

    }

    public void initElementos(){

        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();

        headeCuenta = (RelativeLayout) findViewById(R.id.headeColor);
        teamIcon = (ImageView) findViewById(R.id.teamIdentifier);

        c_amigos = (TextView) findViewById(R.id.count_friends);
        accounts = (TextView) findViewById(R.id.cuentas_activas);

        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("usuarios");

        swipeRefreshLayout = findViewById(R.id.accountRefresh);

        cuentapkmn = 0;

        formAccount = findViewById(R.id.form_account);
        formMatch = findViewById(R.id.form_trade);

        cuenta = formAccount.findViewById(R.id.user_trainer);
        codigo = formAccount.findViewById(R.id.code_trainer);
        nivel = formAccount.findViewById(R.id.lvl_trainer);
        equipo = formAccount.findViewById(R.id.team_trainer);
        poke = formMatch.findViewById(R.id.select_pkmn);
        descripcion = formMatch.findViewById(R.id.descripcion_trade);
        pick = formMatch.findViewById(R.id.pickImage);

        pokelist = formMatch.findViewById(R.id.lista_busca);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,R.array.pkmn_team_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        equipo.setAdapter(arrayAdapter);

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Pkdx.getList());
        arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        poke.setAdapter(arrayAdapter2);

        SharedPreferences team = this.getSharedPreferences(AppConstants.userData,this.MODE_PRIVATE);
        int color = team.getInt("team",0);

        switch (color){
            case 1:
                headeCuenta.setBackgroundColor(getResources().getColor(R.color.teamInstinto));
                teamIcon.setImageResource(R.drawable.ic_instinto);
                break;
            case 2:
                headeCuenta.setBackgroundColor(getResources().getColor(R.color.teamValor));
                teamIcon.setImageResource(R.drawable.ic_valor);
                break;
            case 3:
                headeCuenta.setBackgroundColor(getResources().getColor(R.color.teamSabiduria));
                teamIcon.setImageResource(R.drawable.ic_sabiduria);
                break;
            case 0:
                PrincipalMethods.chooseTeam(this,this);
                break;
        }

        floatingActionMenu = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);
        f1 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item1);
        f2 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item2);
        f3 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item3);

        f1.setOnClickListener(this);
        f2.setOnClickListener(this);
        f3.setOnClickListener(this);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFirebaseData();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        poke.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem = adapterView.getItemAtPosition(i).toString();
                cuentapkmn++;
                if (!selectedItem.contains("Selecciona") && cuentapkmn <= 5) {
                    pokelist.setText(pokelist.getText().toString() + selectedItem + ", ");
                } else if (cuentapkmn>5){
                    Toast.makeText(CuentaActivity.this, "SOLO PUEDES SELECCIONAR 4 POKEMON.", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void onClickMenu(View view){
        DrawerCreator.openDrawer();
    }

    public void onClickClose(View view){
        startActivity(new Intent(this,PrincipalActivity.class));
        finish();

    }

    public void checkForm(){
        int val = 0;
        if (PrincipalMethods.checkVacios(this, cuenta)){val++;}
        if (PrincipalMethods.checkVacios(this, nivel)){val++;}
        if (PrincipalMethods.checkVacios(this, codigo)){val++;}
        if (equipo.getSelectedItemId() != 0){val++;}

        if (val>=4){
            sendToFBase();
        }

    }

    public void sendToFBase(){
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        mReference.child(user.getUid()).child("pkmAccounts").child(cuenta.getText().toString()).setValue("activo");
        mReference.child(user.getUid()).child("pkmnFriend").child(cuenta.getText().toString()).child("pkmncode").setValue(codigo.getText().toString());
        mReference.child(user.getUid()).child("pkmnFriend").child(cuenta.getText().toString()).child("pkmnlvl").setValue(nivel.getText().toString());
        mReference.child(user.getUid()).child("pkmnFriend").child(cuenta.getText().toString()).child("pkmnteam").setValue(equipo.getSelectedItem().toString());
        mReference.child(user.getUid()).child("pkmnFriend").child(cuenta.getText().toString()).child("pkmnuid").setValue(cuenta.getText().toString());

        mReference.child("amigos").child(cuenta.getText().toString()).child("pkmncode").setValue(codigo.getText().toString());
        mReference.child("amigos").child(cuenta.getText().toString()).child("pkmnlvl").setValue(nivel.getText().toString());
        mReference.child("amigos").child(cuenta.getText().toString()).child("pkmnteam").setValue(equipo.getSelectedItem().toString());
        mReference.child("amigos").child(cuenta.getText().toString()).child("pkmnuid").setValue(cuenta.getText().toString());
        mReference.child("amigos").child(cuenta.getText().toString()).child("mainuid").setValue(user.getUid());
        mReference.child("amigos").child(cuenta.getText().toString()).child("visible").setValue("true");

        Toast.makeText(this, "¡CUENTA AÑADIDA CORRECTAMENTE!", Toast.LENGTH_SHORT).show();
        animForm(0, formAccount);

    }

    public void onClickForm(View view){

        switch (view.getId()){
            case R.id.ok_buttonform:
                checkForm();
                break;
            case R.id.cancel_buttonform:
                animForm(0, formAccount);
                break;
            case R.id.pickImage:
                pickFromGallery();
                break;
            case R.id.ok_buttonform2:
                uploadImage();
                break;
            case R.id.cancel_buttonform2:
                animForm(0, formMatch);
                break;
        }

    }

    public void getFirebaseData(){

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        mReference.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("ResponseC",dataSnapshot.toString());
                int count = 0;

                //Muestra cuentas activas
                if (dataSnapshot.child("pkmAccounts").exists()){
                    String cuentas = "";
                    for (DataSnapshot snapshot : dataSnapshot.child("pkmAccounts").getChildren()){
                        Log.d("pkAcc2",snapshot.toString());
                        cuentas += snapshot.getKey()+"\n";
                    }
                    accounts.setText(cuentas);
                }

                //Cuenta codigo copiado
                if (dataSnapshot.child("pkmnFriend").exists()){
                    for (DataSnapshot snapshot : dataSnapshot.child("pkmnFriend").getChildren()){
                        if (snapshot.child("solicitud").exists()){
                            count = count+ (int) snapshot.child("solicitud").getChildrenCount();
                        }
                    }
                }
                c_amigos.setText("Código copiado: "+count+" veces.");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void animForm(final int tipo, View target){

        Transition transition = new Fade();
        transition.setDuration(200);
        transition.addTarget(target);

        ViewGroup main = findViewById(R.id.mainlayout_account);

        TransitionManager.beginDelayedTransition(main, transition);
        if (tipo == 1) {
            target.setVisibility(View.VISIBLE);
        } else {
            target.setVisibility(View.GONE);
        }

    }

    public void pickFromGallery(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        String[] mimeTypes = {"image/jpge", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(Intent.createChooser(intent, "Selecciona una captura de pantalla del pokemon que ofreces"), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null){
            uri = data.getData();
            if (uri != null){
                pick.setText("CAPTURA SELECCIONADA");
                pick.setEnabled(false);
            } else {
                pick.setText("Error, selecciona...");
                pick.setTextColor(getResources().getColor(R.color.md_red_A700));
            }
        }

    }

    public void uploadImage(){
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        if (uri != null && !TextUtils.isEmpty(pokelist.getText())){
            Bitmap imagenC;
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            Bitmap imageBitmap = null;
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
                imagenC = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
                imageBitmap = Bitmap.createScaledBitmap(imagenC,(int)(imagenC.getWidth()*0.6), (int)(imagenC.getHeight()*0.6), true);
            } catch (IOException e) {
                e.printStackTrace();
            }

            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

            byte[] bytes1 = bytes.toByteArray();

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Subiendo...");
            progressDialog.show();

            final StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());
            ref.putBytes(bytes1)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String id = UUID.randomUUID().toString();
                                    mReference = mDatabase.getReference().child("usuarios").child(user.getUid());
                                    mReference.child("pkmnMatches").child("disponibles").child(id).setValue(pokelist.getText().toString());
                                    mReference.child("pkmnMatches").child(id).setValue(id);
                                    mReference = mDatabase.getReference().child("matches");
                                    mReference.child(id).child("descripcion").setValue(descripcion.getText().toString());
                                    mReference.child(id).child("usuario").setValue(user.getUid());
                                    mReference.child(id).child("pkmns").setValue(pokelist.getText().toString());
                                    mReference.child(id).child("uidmatch").setValue(id);
                                    mReference.child(id).child("imagen").setValue(uri.toString());
                                    Log.d("DOWNLOADURL", uri.toString());
                                    progressDialog.dismiss();
                                    descripcion.setText("");
                                    pokelist.setText("");
                                    uri = null;
                                    cuentapkmn = 0;
                                    pick.setText("SELECCIONA SCREENSHOT");
                                    pick.setEnabled(true);
                                    Toast.makeText(CuentaActivity.this, "Cargada", Toast.LENGTH_SHORT).show();
                                    animForm(0,formMatch);
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(CuentaActivity.this, "Falló carga, reintente... ", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Subiendo "+(int)progress+"%");
                        }
                    });
        } else if (TextUtils.isEmpty(pokelist.getText())){
            Toast.makeText(this, "¡DEBES SELECCIONAR AL MENOS UN POKEMON!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "¡NECESITAS ADJUNTAR UNA CAPTURA DE PANTALLA DEL POKEMON QUE OFRECES!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.material_design_floating_action_menu_item1:
                floatingActionMenu.close(true);
                animForm(1, formAccount);
                break;
            case R.id.material_design_floating_action_menu_item2:
                floatingActionMenu.close(true);
                animForm(1, formMatch);
                break;
        }
    }
}
