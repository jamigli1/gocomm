package com.jamiglico.gocomunidad.Interactor;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jamiglico.gocomunidad.Model.Trainer;
import com.jamiglico.gocomunidad.R;

import java.util.List;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private Context context;
    private List<Trainer> trainers;

    public FriendsAdapter(Context context, List<Trainer> trainers) {
        this.context = context;
        this.trainers = trainers;
    }

    @Override
    public FriendsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(context).inflate(R.layout.single_friend, parent, false);
        return new FriendsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendsAdapter.ViewHolder holder, int position){
        Trainer trainer = trainers.get(position);
        holder.user.setText(trainer.getPkmnuid());
        holder.level.setText("Nvl: "+trainer.getPkmnlvl());
        holder.codigo.setText(trainer.getPkmncode());
//        holder.imagen.setBackgroundColor(Color.parseColor(trainer.getPkmncolor()));
    }

    @Override
    public int getItemCount(){
        return trainers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView user, codigo, level;
        public ImageView imagen;

        public ViewHolder(View itemView){
            super(itemView);
            codigo = itemView.findViewById(R.id.frnd_trainercode);
            user = itemView.findViewById(R.id.frn_trainername);
            level = itemView.findViewById(R.id.frn_trainerlvl);
            imagen = itemView.findViewById(R.id.img_friend);
        }

    }
}
