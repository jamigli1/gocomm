package com.jamiglico.gocomunidad.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jamiglico.gocomunidad.CuentaActivity;
import com.jamiglico.gocomunidad.FriendsActivity;
import com.jamiglico.gocomunidad.LoginActivity;
import com.jamiglico.gocomunidad.NidosActivity;
import com.jamiglico.gocomunidad.PrincipalActivity;
import com.jamiglico.gocomunidad.R;
import com.jamiglico.gocomunidad.TradeActivity;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

public class DrawerCreator {

    private static DrawerBuilder drawerBuilder;
    private static Drawer openD;

    public static void createDrawer(final Activity activity, final Toolbar toolbar){

        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = mAuth.getCurrentUser();

        PrimaryDrawerItem item1 = new PrimaryDrawerItem()
                .withIdentifier(1)
                .withName("Nidos actuales")
                .withIcon(activity.getResources().getDrawable(R.drawable.ic_nidos));
        PrimaryDrawerItem item2 = new PrimaryDrawerItem()
                .withIdentifier(2)
                .withName("Amigos")
                .withIcon(activity.getResources().getDrawable(R.drawable.ic_amigos));
        PrimaryDrawerItem item3 = new PrimaryDrawerItem()
                .withIdentifier(3)
                .withName("Intercambios")
                .withIcon(activity.getResources().getDrawable(R.drawable.ic_intercambio));
        PrimaryDrawerItem item4 = new PrimaryDrawerItem()
                .withIdentifier(4)
                .withName("Eventos")
                .withIcon(activity.getResources().getDrawable(R.drawable.ic_friends));
        PrimaryDrawerItem item5 = new PrimaryDrawerItem()
                .withIdentifier(5)
                .withName("Cuenta")
                .withIcon(activity.getResources().getDrawable(R.drawable.ic_account));
        SecondaryDrawerItem itemAbout = new SecondaryDrawerItem()
                .withIdentifier(98)
                .withName("Acerca de...")
                .withIcon(activity.getResources().getDrawable(R.drawable.ic_info));
        SecondaryDrawerItem itemLogout = new SecondaryDrawerItem()
                .withIdentifier(99)
                .withName("Cerrar sesión")
                .withIcon(activity.getResources().getDrawable(R.drawable.ic_logout));

        final AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(activity)

                .withHeaderBackground(getTeam(activity))
                .withTextColorRes(R.color.colorWhite)
                .addProfiles(
                        new ProfileDrawerItem()
                                .withName(user.getEmail())
                                .withEmail("PKMN TRAINER")
                                .withIcon(activity.getResources().getDrawable(R.drawable.ic_launcher_foreground))
                )
                .build();

        drawerBuilder = new DrawerBuilder()
                .withActivity(activity)
                .withAccountHeader(accountHeader)
                .withSelectedItem(-1)
                .withToolbar(toolbar)
                .withTranslucentNavigationBar(false)
                .withActionBarDrawerToggle(false)
                .addDrawerItems(
                        item1,
                        item2,
                        item3,
                        item4,
                        item5,
                        new DividerDrawerItem(),
                        itemAbout,
                        itemLogout
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        if (drawerItem != null){
                            if (drawerItem.getIdentifier() == 1){
                                activity.startActivity(new Intent(activity, NidosActivity.class));
                                finishAct(activity);
                            } else if (drawerItem.getIdentifier() == 2){
                                activity.startActivity(new Intent(activity, FriendsActivity.class));
                                finishAct(activity);
                            } else if (drawerItem.getIdentifier() == 3){
                                activity.startActivity(new Intent(activity, TradeActivity.class));
                                finishAct(activity);
                            } else if (drawerItem.getIdentifier() == 4){
                                Toast.makeText(activity, "seleccionado 3", Toast.LENGTH_SHORT).show();
                            } else if (drawerItem.getIdentifier() == 5){
                                activity.startActivity(new Intent(activity, CuentaActivity.class));
                                finishAct(activity);
                            } else if (drawerItem.getIdentifier() == 98){
                                try {
                                    PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(),0);
                                    Toast.makeText(activity, "Versión: "+packageInfo.versionName, Toast.LENGTH_SHORT).show();
                                } catch (PackageManager.NameNotFoundException e){
                                    e.printStackTrace();
                                }
                            } else if (drawerItem.getIdentifier() == 99){
                                mAuth.signOut();
                                activity.getSharedPreferences(AppConstants.userData, Context.MODE_PRIVATE).edit().clear().apply();
                                activity.finish();
                                activity.startActivity(new Intent(activity, LoginActivity.class));
                            }
                        }

                        return false;
                    }
                });

        toolbar.setBackgroundColor(activity.getResources().getColor(getTeam(activity)));
        openD = drawerBuilder.build();

        if (Build.VERSION.SDK_INT >= 21){
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(activity.getResources().getColor(getTeam(activity)));
        }

    }

    public static void finishAct(Activity activity){
        if (activity.getClass() != PrincipalActivity.class){
            activity.finish();
        }
    }

    public static void openDrawer(){
        openD.openDrawer();
    }

    private static int getTeam(Activity activity){
        SharedPreferences team = activity.getSharedPreferences(AppConstants.userData,activity.MODE_PRIVATE);
        int color = team.getInt("team",0);

        switch (color){
            case 1:
                return R.color.teamInstinto;
            case 2:
                return R.color.teamValor;
            case 3:
                return R.color.teamSabiduria;
            case 0:
                return R.color.teamSabiduria;
        }

        return color;
    }

}
