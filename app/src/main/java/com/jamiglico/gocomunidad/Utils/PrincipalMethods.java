package com.jamiglico.gocomunidad.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jamiglico.gocomunidad.R;

public class PrincipalMethods {

    public static void chooseTeam(Activity activity, Context context){

        SharedPreferences team = activity.getSharedPreferences(AppConstants.userData, activity.MODE_PRIVATE);
        final SharedPreferences.Editor editor = team.edit();

        View mView = activity.getLayoutInflater().inflate(R.layout.team_selector, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setView(mView);

        final Dialog dialog;
        dialog = builder.create();

        ImageView sabiduria = (ImageView) mView.findViewById(R.id.img_sabi);
        ImageView instinto = (ImageView) mView.findViewById(R.id.img_insti);
        ImageView valor = (ImageView) mView.findViewById(R.id.img_valor);

        instinto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("team",1);
                editor.putInt("first",1);
                editor.apply();
                dialog.dismiss();
            }
        });

        sabiduria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("team",3);
                editor.putInt("first",1);
                editor.apply();
                dialog.dismiss();
            }
        });

        valor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.putInt("team",2);
                editor.putInt("first",1);
                editor.apply();
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public static boolean checkVacios(Context context, TextView textView){
        if (TextUtils.isEmpty(textView.getText())){
            textView.setTextColor(context.getResources().getColor(R.color.toppkbl));
            textView.setError("CAMPO REQUERIDO");
            return false;
        }
        return true;
    }

}
