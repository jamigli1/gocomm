package com.jamiglico.gocomunidad;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.jamiglico.gocomunidad.Utils.AppConstants;
import com.jamiglico.gocomunidad.Utils.DrawerCreator;
import com.jamiglico.gocomunidad.Utils.PrincipalMethods;

public class PrincipalActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        chooseTeam();
        DrawerCreator.createDrawer(this, toolbar);

    }

    public void onClickMenu(View view){
        DrawerCreator.openDrawer();
    }

    public void chooseTeam(){
        SharedPreferences team = this.getSharedPreferences(AppConstants.userData,this.MODE_PRIVATE);
        if (team.getInt("first",0) == 0) {
            PrincipalMethods.chooseTeam(this, this);
        }
    }

    public void onClickCards(View view){

        switch (view.getId()){
            case R.id.principal_opc1:
                startActivity(new Intent(this, NidosActivity.class));
                break;
            case R.id.principal_opc2:
                startActivity(new Intent(this, FriendsActivity.class));
                break;
            case R.id.principal_opc3:
                startActivity(new Intent(this, TradeActivity.class));
                break;
            case R.id.principal_opc4:
                Toast.makeText(this, "OPC4", Toast.LENGTH_SHORT).show();
                break;
            case R.id.principal_opc5:
                startActivity(new Intent(this, CuentaActivity.class));
                break;
        }

    }

}
