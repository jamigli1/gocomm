package com.jamiglico.gocomunidad.Model;

import android.graphics.Color;

public class Trainer {

    private String mainuid, pkmncode, pkmncolor, pkmnlvl, pkmnteam, pkmnuid, visible;

    public Trainer() {
    }

    public Trainer(String pkmnuid, String pkmnlvl, String pkmnteam, String pkmncode, String mainuid, String visible, String pkmncolor) {
        this.pkmnuid = pkmnuid;
        this.pkmncode = pkmncode;
        this.pkmncolor = pkmncolor;
        this.pkmnlvl = pkmnlvl;
        this.pkmnteam = pkmnteam;
        this.mainuid = mainuid;
        this.visible = visible;

    }

    public String getPkmnuid() {
        return pkmnuid;
    }

    public void setPkmnuid(String pkmnuid) {
        this.pkmnuid = pkmnuid;
    }

    public String getPkmnlvl() {
        return pkmnlvl;
    }

    public void setPkmnlvl(String pkmnlvl) {
        this.pkmnlvl = pkmnlvl;
    }

    public String getPkmnteam() {
        return pkmnteam;
    }

    public void setPkmnteam(String pkmnteam) {
        this.pkmnteam = pkmnteam;
    }

    public String getPkmncode() {
        return pkmncode;
    }

    public void setPkmncode(String pkmncode) {
        this.pkmncode = pkmncode;
    }

    public String getMainuid() {
        return mainuid;
    }

    public void setMainuid(String mainuid) {
        this.mainuid = mainuid;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getPkmncolor() {
        return pkmncolor;
    }

    public void setPkmncolor(String pkmncolor) {
        this.pkmncolor = pkmncolor;
    }
}
