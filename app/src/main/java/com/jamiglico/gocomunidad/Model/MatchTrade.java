package com.jamiglico.gocomunidad.Model;

public class MatchTrade {

    private String descripcion, imagen, pkmns, uidmatch, usuario;

    public MatchTrade(String descripcion, String imagen, String pkmns, String uidmatch, String usuario) {
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.pkmns = pkmns;
        this.uidmatch = uidmatch;
        this.usuario = usuario;
    }

    public MatchTrade() {
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getPkmns() {
        return pkmns;
    }

    public void setPkmns(String pkmns) {
        this.pkmns = pkmns;
    }

    public String getUidmatch() {
        return uidmatch;
    }

    public void setUidmatch(String uidmatch) {
        this.uidmatch = uidmatch;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
