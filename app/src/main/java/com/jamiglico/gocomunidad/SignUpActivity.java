package com.jamiglico.gocomunidad;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView email, pass1, pass2, pkmnuser;
    private Button registrar;
    private ProgressBar carga;

    private DatabaseReference mReference;
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initElementos();

        registrar.setOnClickListener(this);

    }

    private void initElementos(){

        email = (TextView) findViewById(R.id.sg_correo);
        pass1 = (TextView) findViewById(R.id.sg_pass);
        pass2 = (TextView) findViewById(R.id.sg_passConf);
        pkmnuser = (TextView) findViewById(R.id.sg_pkmnuser);
        registrar = (Button) findViewById(R.id.registrar);
        carga = (ProgressBar) findViewById(R.id.sg_progressbar);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("usuarios");
    }

    public void registrarUser(){

        if (checkCampos() && checkPass()){
            solicitaRegistro(email.getText().toString(), pass1.getText().toString(), pkmnuser.getText().toString());
        }

    }

    public boolean checkCampos(){
        int cont = 0;
        if (!TextUtils.isEmpty(email.getText())){
            cont++;
        } else {
            email.setError("CAMPO REQUERIDO");
        }

        if (!TextUtils.isEmpty(pkmnuser.getText())){
            cont++;
        } else {
            email.setError("CAMPO REQUERIDO");
        }

        if (!TextUtils.isEmpty(pass1.getText())){
            cont++;
        } else {
            email.setError("CAMPO REQUERIDO");
        }

        if (!TextUtils.isEmpty(pass2.getText())){
            cont++;
        } else {
            email.setError("CAMPO REQUERIDO");
        }

        if (cont != 4){
            return false;
        } else {
            return true;
        }
    }

    public boolean checkPass(){
        if(TextUtils.equals(pass1.getText(),pass2.getText())){
            return true;
        } else {
            pass1.setError("CONTRASEÑA NO COINCIDE");
            pass2.setError("CONTRASEÑA NO COINCIDE");
            return false;
        }
    }

    public void solicitaRegistro(final String correo, String password, final String pkuser){

        carga.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(correo, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser usuario = mAuth.getCurrentUser();
                            mReference.child(usuario.getUid()).child("pkmAccounts").child(pkuser).setValue("activo");
                            mAuth.signOut();
                            carga.setVisibility(View.GONE);
                            startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                            finish();
                        } else {
                            Toast.makeText(SignUpActivity.this, "NO SE HA PODIDO CREAR EL USUARIO, REINTENTE.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.registrar:
                registrarUser();
                break;
        }
    }
}
