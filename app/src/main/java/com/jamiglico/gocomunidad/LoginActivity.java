package com.jamiglico.gocomunidad;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText email, pass;
    private Button iniciar;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initElementos();

        iniciar.setOnClickListener(this);

    }

    private void iniciaSesion(){

        if (checkCampos()){
            mAuth.signInWithEmailAndPassword(email.getText().toString(), pass.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                startActivity(new Intent(LoginActivity.this, PrincipalActivity.class));
                                finish();
                            } else {
                                Toast.makeText(LoginActivity.this, "ERROR AL INICIAR SESIÓN, VERIFICA TUS DATOS.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }

    public boolean checkCampos(){
        int cont = 0;
        if (!TextUtils.isEmpty(email.getText())){
            cont++;
        } else {
            email.setError("INGRESE SU CORREO");
        }

        if (!TextUtils.isEmpty(pass.getText())){
            cont++;
        } else {
            pass.setError("INGRESE SU CONTRASEÑA");
        }

        if (cont==2){
            return true;
        }

        return false;

    }

    public void initElementos(){

        email = (EditText) findViewById(R.id.lg_email);
        pass = (EditText) findViewById(R.id.lg_password);
        iniciar = (Button) findViewById(R.id.lg_iniciar);
        progressBar = (ProgressBar) findViewById(R.id.lg_cargar);

        mAuth = FirebaseAuth.getInstance();
    }

    public void onClickSignUp(View view){
        startActivity(new Intent(this, SignUpActivity.class));
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lg_iniciar:
                iniciaSesion();
                break;
        }
    }
}
