package com.jamiglico.gocomunidad;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jamiglico.gocomunidad.Interactor.FriendsAdapter;
import com.jamiglico.gocomunidad.Model.Trainer;
import com.jamiglico.gocomunidad.Utils.DrawerCreator;
import com.jamiglico.gocomunidad.Utils.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class FriendsActivity extends AppCompatActivity {

    private DatabaseReference mReference;
    private FirebaseDatabase mDatabase;

    private Toolbar toolbar;

    private List<Trainer> trainerList;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView mList;
    private DividerItemDecoration dividerItemDecoration;
    private LinearLayoutManager linearLayoutManager;
    private ProgressDialog progressDialog;
    private ClipboardManager clipboardManager;
    private ClipData clipData;
    private FirebaseAuth auth;
    private FirebaseUser user;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        toolbar = (Toolbar) findViewById(R.id.toolbarFriends);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerCreator.createDrawer(this, toolbar);

        initElementos();

        getFirebaseData();

    }

    @Override
    public void onBackPressed(){
        finish();
    }

    public void addTouch(){
        mList.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                AlertDialog.Builder builder = new AlertDialog.Builder(FriendsActivity.this);
                builder.setMessage("¿Copiar este código de entrenador?");
                builder.setCancelable(true);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        copyCode(trainerList.get(position).getPkmncode(), position);
                    }
                });
                builder.show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    public void initElementos(){
        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference().child("usuarios").child("amigos");

        mList = (RecyclerView) findViewById(R.id.recycler_friends);
        trainerList = new ArrayList<>();
        mAdapter = new FriendsAdapter(getApplicationContext(),trainerList);

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());

        swipeRefreshLayout = findViewById(R.id.friends_refresh);

        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(mAdapter);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("CARGANDO DATOS...");

        clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        progressDialog.show();

        addTouch();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        trainerList.clear();
                        mAdapter.notifyDataSetChanged();
                        getFirebaseData();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 2000);
            }
        });

    }

    public void getFirebaseData(){

        final String random = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
        final int rndm = (int) (Math.random() * 24);

        mReference.limitToFirst(200).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    Trainer trainer = dataSnapshot1.getValue(Trainer.class);
                    if (!trainer.getMainuid().equals(auth.getUid()) && trainer.getPkmnuid().contains(String.valueOf(random.charAt(rndm)))) {
                        trainerList.add(trainer);
                    }
                }
                progressDialog.dismiss();
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("Error",databaseError.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    public void onClickMenu(View view){
        DrawerCreator.openDrawer();
    }

    public void copyCode(String codigo, int position){
        clipData = ClipData.newPlainText("text",codigo);
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(this, "¡Código de entrenador copiado!", Toast.LENGTH_SHORT).show();

        mDatabase.getReference().child("usuarios")
                .child(trainerList.get(position).getMainuid())
                .child("pkmnFriend")
                .child(trainerList.get(position).getPkmnuid())
                .child("solicitud")
                .child(user.getUid())
                .setValue(user.getUid());

    }

}
