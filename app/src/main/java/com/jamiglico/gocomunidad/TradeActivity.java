package com.jamiglico.gocomunidad;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.transition.Fade;
import androidx.transition.Slide;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jamiglico.gocomunidad.Model.MatchTrade;
import com.jamiglico.gocomunidad.Model.Trainer;
import com.jamiglico.gocomunidad.Utils.DrawerCreator;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TradeActivity extends AppCompatActivity {

    private View view, match, completo;
    private TextView textCargador;
    private Toolbar toolbar;

    private TextView descripcion, pokelist;
    private ImageView screenshot;

    private DatabaseReference mReference;
    private FirebaseDatabase mDatabase;
    private FirebaseAuth auth;
    private FirebaseUser user;

    private ProgressDialog progressDialog;

    private int count = 0;

    private List<MatchTrade> matchTradeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trade);

        toolbar = findViewById(R.id.toolbarTrade);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerCreator.createDrawer(this,toolbar);

        initElementos();

        getFirebaseData();

    }

    public void initElementos(){
        view = findViewById(R.id.cargadorLayout);
        match = findViewById(R.id.matchLayout);
        textCargador = view.findViewById(R.id.textCargador);
        completo = findViewById(R.id.layout_completo);

        descripcion = findViewById(R.id.ubicacionAll_trade);
        pokelist = findViewById(R.id.pkmnlist_trade);
        screenshot = findViewById(R.id.image_trade_trade);

        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("CARGANDO DATOS...");

        progressDialog.show();

        matchTradeList = new ArrayList<>();
    }

    public void onClickBotones(View v){
        switch (v.getId()){
            case R.id.dislike:
                textCargador.setText("NO ME INTERESA");
                transResponse(1);
                break;
            case R.id.like:
                textCargador.setText("¡LO QUIERO!");
                transResponse(0);
                break;
        }
    }

    public void matchLayout(){
        Transition transition = new Slide(Gravity.BOTTOM);
        transition.setDuration(200);
        transition.addTarget(match);

        ViewGroup main = findViewById(R.id.mainlayout);

        TransitionManager.beginDelayedTransition(main, transition);
        match.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Transition transition = new Slide(Gravity.TOP);
                transition.setDuration(400);
                transition.addTarget(match);

                ViewGroup main = findViewById(R.id.mainlayout);

                TransitionManager.beginDelayedTransition(main, transition);
                match.setVisibility(View.GONE);
            }
        },1500);
    }

    public void transResponse(final int tipo){

        Transition transition = new Fade();
        transition.setDuration(200);
        transition.addTarget(view);

        ViewGroup main = findViewById(R.id.mainlayout);

        TransitionManager.beginDelayedTransition(main, transition);
        view.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Transition transition = new Fade();
                transition.setDuration(400);
                transition.addTarget(view);

                ViewGroup main = findViewById(R.id.mainlayout);

                TransitionManager.beginDelayedTransition(main, transition);
                view.setVisibility(View.GONE);
                cambiaDatos(tipo);
            }
        },1200);
    }

    public void cambiaDatos(int tipo){
        if (tipo == 1) {
            Animation startRotation = AnimationUtils.loadAnimation(this, R.anim.anim_dislike);
            completo.startAnimation(startRotation);
            count++;
            setData(count);
        } else {
            Animation startRotation = AnimationUtils.loadAnimation(this, R.anim.anim_like);
            completo.startAnimation(startRotation);
            count++;
            setData(count);
        }
    }

    public void getFirebaseData(){
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();

        mReference.child("matches").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d("DataXDDD", dataSnapshot.toString());
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    MatchTrade matchTrade = dataSnapshot1.getValue(MatchTrade.class);
                    if (!matchTrade.getUsuario().equals(auth.getUid())) {
                        matchTradeList.add(matchTrade);
                    }
                }
                progressDialog.dismiss();
                setData(0);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("Error",databaseError.getMessage());
            }
        });
    }

    public void setData(int i){
        if (matchTradeList.size()>0 && i<matchTradeList.size()) {
            Picasso.get().load(matchTradeList.get(i).getImagen()).into(screenshot);
            descripcion.setText(matchTradeList.get(i).getDescripcion());
            pokelist.setText(matchTradeList.get(i).getPkmns());
        } else {
            screenshot.setImageResource(R.drawable.ic_nodata);
            descripcion.setText("NO HAY MÁS PROPUESTAS DISPONIBLES :(");
            pokelist.setText("¡VUELVE PRONTO!");
        }
    }
}
